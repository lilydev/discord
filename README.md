# Assets/YAML files for Lilydev's Discord server

## Licensing

Everything except the files under the `Assets` directory is licensed under [GNU General Public License v3.0](https://codeberg.org/lilydev/discord/src/branch/main/LICENSE), while the files under 'Assets' are `All Rights Reserved`.
